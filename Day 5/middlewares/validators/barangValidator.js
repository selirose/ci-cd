const {
    barang,
    pemasok
} = require('../../models')
const {
    check,
    validationResult,
    matchedData,
    sanitize
} = require('express-validator');

const multer = require('multer')
const path = require('path')
const crypto = require('crypto')

const uploadDir = '/img/'
const storage = multer.diskStorage({
    destination: './public' + uploadDir,
    filename: function(req, file, cb) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if(err) return cb(err)

            cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
    }
})

const upload = multer({ storage: storage, dest: uploadDir})


module.exports = {
    getOne: [
        check('id').custom(value => {
            return barang.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('ID barang tidak ada!')
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        },
    ],
    create: [
        upload.single('image'),
        check('id_pemasok').custom(value => {
            return pemasok.findOne({
                _id: value
            }).then(b => {
                if (!b) {
                    throw new Error('ID pemasok tidak ada!');
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    update: [
        upload.single('image'),
        check('id').custom(value => {
            return barang.findOne({
                _id: value
            }).then(b => {
                if (!b) {
                    throw new Error('ID barang tidak ada!');
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    delete: [
        check('id').custom(value => {
          return barang.findOne({
            _id: value
          }).then(result => {
            if (!result) {
              throw new Error('ID barang tidak ada!')
            }
          })
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        }
    ]
}