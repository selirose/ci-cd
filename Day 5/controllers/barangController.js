const {
    barang,
    pemasok
} = require('../models')

class BarangController {
    async getAll(req, res) {
        barang.find({}).then(result => {
            res.json({
                status: "success",
                data: result
            })
        });
    }

    async getOne(req, res) {
        barang.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        const data = await Promise.all([
            pemasok.findOne({
                _id: req.body.id_pemasok
            })
        ])

        barang.create({
            pemasok: data[0],
            nama: req.body.nama,
            harga: req.body.harga,
            image: req.file === undefined ? "" : req.file.filename
        }).then((result) => {
            res.json({
                status: 'success',
                data: result
            })
        })
    }

    async delete(req, res) {
        barang.delete({
            _id: req.params.id
        }).then(() => {
            res.json({
                status: "success",
                data: null
            })
        })
    }

    async update(req, res) {
        const data = await Promise.all([
            pemasok.findOne({
                _id: req.body.id_pemasok
            })
        ])
        barang.findOneAndUpdate({
            _id: req.params.id
        }, {
            "pemasok": data[0],
            "nama": req.body.nama,
            "harga": req.body.harga,
            image: req.file === undefined ? "" : req.file.filename
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })


    }
}
module.exports = new BarangController;