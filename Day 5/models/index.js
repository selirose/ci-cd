const mongoose = require('mongoose');

let uri = "mongodb://localhost:27017/penjualan_dev"

if (process.env.APP_ENV == "test") {
    uri = "mongodb://localhost:27017/penjualan_test"
}

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true })
.then(() => console.log(`MongoDB Connected! uri: ${uri}`))
.catch(err => console.log(err))

const barang = require('./barang.js')
const pelanggan = require('./pelanggan.js')
const pemasok = require('./pemasok')
const transaksi = require('./transaksi.js')

module.exports = { barang, pelanggan, pemasok, transaksi };
