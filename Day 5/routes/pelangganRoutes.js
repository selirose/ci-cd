const express = require('express') // Import express
const router = express.Router() // Make router from app
const PelangganController = require('../controllers/pelangganController.js') 
const PelangganValidator = require('../middlewares/validators/pelangganValidator.js') 

router.get('/', PelangganController.getAll) 
router.get('/:id', PelangganController.getOne) 
router.post('/create', PelangganValidator.create, PelangganController.create) 
router.put('/update/:id', PelangganValidator.update, PelangganController.update) 
router.delete('/delete/:id', PelangganController.delete) 

module.exports = router; // Export router
